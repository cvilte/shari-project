﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Proyecto.Models
{
    public class Question
    {   [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        
        public string Description { get; set; }
        public int CorrectAnswerID { get; set; }
        public virtual ICollection<Answer> Answers { get; set; }

    }
}