﻿using System.Linq;
using System.Web.Mvc;
using Proyecto.DAL;

namespace Proyecto.Controllers
{
  public class QuestionsController : Controller
  {
    private ProjectContext db = new ProjectContext();

    // GET: Questions
    public ActionResult Index()
    {
      return View(db.Questions.ToList());
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        db.Dispose();
      }
      base.Dispose(disposing);
    }

    [HttpPost]
    public ActionResult DoSend(string prueba)
    {
      /*
      Session["name"] = name;
      Session["age"] = age;
      Session["nationality"] = nationality;
      Session["occupation"] = Occupation;

      return RedirectToAction("Index", "Questions");
      */
      return View();
    }
  }
}
