﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using Proyecto.Models;
using Proyecto.DAL;

namespace Proyecto.Controllers
{
  public class HomeController : Controller
  {
    private ProjectContext db = new ProjectContext();

    public ActionResult Index()
    {
      return View();
    }

    public ActionResult About()
    {
      ViewBag.Message = "Your application description page.";

      return View();
    }

    public ActionResult Test()
    {
      ViewBag.Message = "Start test";
      return View();
    }

    public ActionResult Contact()
    {
      ViewBag.Message = "Your contact page.";

      return View();
    }
    [HttpPost]
    public ActionResult RecibirContacto(string name, string mail, string message)
    {
      //return RedirectToAction("Index", "Home");
      ViewBag.Name = name;
      ViewBag.Mail = mail;
      ViewBag.Message = message;


      SmtpClient clienteSmtp = new SmtpClient("smtp.gmail.com", 587);
      clienteSmtp.Credentials = new NetworkCredential("gender.rolesproject@gmail.com", "32021174G&R");
      clienteSmtp.EnableSsl = true;

      MailMessage mailParaAdministrador = new MailMessage();
      mailParaAdministrador.From = new MailAddress("gender.rolesproject@gmail.com", "Gender Roles");
      mailParaAdministrador.To.Add("gender.rolesproject@gmail.com");
      mailParaAdministrador.Subject = "Nuevo contacto";
      mailParaAdministrador.Body = "Te contactó: " + name + "(" + mail + ").\nSu mensaje fue: " + message;

      clienteSmtp.Send(mailParaAdministrador);

      MailMessage mailAUsuario = new MailMessage();
      mailAUsuario.From = new MailAddress("gender.rolesproject@gmail.com", "Gender Roles");
      mailAUsuario.To.Add(mail);
      mailAUsuario.Subject = "Gracias por contactarte con nosotros!";
      mailAUsuario.IsBodyHtml = true;
      mailAUsuario.Body = "Hola <b>" + name + "</b>!<br>Gracias por contactarte con nosotros!<br>Te responderemos a la brevedad.</br>";

      clienteSmtp.Send(mailAUsuario);

      return View("Thanks");
    }

    [HttpPost]
    public ActionResult DoTest(string name, int age, string nationality, string occupation)
    {
      Log log = new Log();
      log.Name = name;
      log.Age = age;
      log.Nationality = nationality;
      log.Occupation = occupation;

      db.Logs.Add(log);
      db.SaveChanges();

      Session["name"] = name;
      Session["age"] = age;
      Session["nationality"] = nationality;
      Session["occupation"] = occupation;

      return RedirectToAction("Index", "Questions");
    }
  }
}