﻿using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.DAL
{
    public class ProjectInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<ProjectContext>
    {
        protected override void Seed(ProjectContext context)
        {
            var answers = new List<Answer>
            {
                new Answer{ID=1,Description="https://pbs.twimg.com/media/CxGA2HQWgAA98dL.jpg"},
                new Answer{ID=2,Description="http://www.espuma.com.ar/wp-content/uploads/2016/04/IMG_4422_cr.jpg"},
                new Answer{ID=3,Description="http://www.elkiosko.com.mx/gif/mujer8.jpg"},
                new Answer{ID=4,Description="http://www3.pictures.zimbio.com/gi/DKNY%2BBackstage%2BFall%2B2010%2BMBFW%2BAguVERCTxpVl.jpg "},
                new Answer{ID=5,Description="http://www.fundacionkonex.org/custom/web/data/imagenes/repositorio/2011/4/20/3000/201603161238201f5e7f2748adabf08629a6312ac3bfdd.jpg"},
                new Answer{ID=6,Description="https://image.ibb.co/jm0dpm/5F7.jpg"},
                new Answer{ID=7,Description="https://cdn.pixabay.com/photo/2017/01/31/21/42/boy-2027487_960_720.png"},
                new Answer{ID=8,Description="https://arualblog.files.wordpress.com/2012/03/silueta-equipo.jpg"},
                new Answer{ID=9,Description="http://www.rgbstock.com/cache1nT3At/users/m/mz/mzacha/300/mli1Q5U.jpg"},
                new Answer{ID=10,Description="http://adiarioconrosario.com/wp-content/uploads/2015/12/cv.jpg"},
                new Answer{ID=11,Description="http://momcircle.com/wp-content/uploads/2013/12/155986857.jpg" },
                new Answer{ID=12,Description="https://www.mujerde10.com/wp-content/uploads/2017/05/portadanetflix.jpg"},
                new Answer{ID=13,Description="https://truimg.toysrus.com/product/images/CBCCE580.zoom.jpg?fit=inside|300:300" },
                new Answer{ID=14,Description="http://cdn.playbuzz.com/cdn/63d226e5-0924-40b5-a136-dabbd97318d0/2ecd3e2c-31ae-4bee-8b45-adcbbd8038cc.jpg" },
                new Answer{ID=15,Description="http://s.cdon.com/media-dynamic/images/product/toy/adventskalendrar/image0/simba_toys_-_new_born_baby_baby_girl_43_cm-16081942-2816205655-xtra.jpg" },
                new Answer{ID=16,Description="http://1.bp.blogspot.com/-fF1pRJhc-Pk/UAfa59IwkyI/AAAAAAAAMbo/FDyM9ybzo58/s1600/3577052-un-ingeniero-de-mantenimiento-hombres-trabajando-en-un-aparato-industrial.jpg"},
                new Answer{ID=17,Description="http://www.mujerglam.cl/wp-content/uploads/2016/02/maite-llorar-x-desamor.jpg"},
                new Answer{ID=18,Description="http://static.ellahoy.es/r/845X0/www.ellahoy.es/img/Pintarse-los-labios-con-pincel.jpg"},
                new Answer{ID=19,Description="https://91b6be3bd2294a24b7b5-da4c182123f5956a3d22aa43eb816232.ssl.cf1.rackcdn.com/contentItem-6679215-54433931-vcq6mfrvpal27-or.png"},
                new Answer{ID=20,Description="https://i.pinimg.com/736x/e2/c1/57/e2c157ef006927cacd2baf8ee9e349a4--travel-kids-car-travel.jpg"},
                new Answer{ID=21,Description="https://i.pinimg.com/736x/fb/4d/49/fb4d4996f743297fbea76bdd4b19863a--new-toys-girls-toys.jpg"},
                new Answer{ID=22,Description="Maternity"},
                new Answer{ID=23,Description="Because they are more sensitive and prefer other activities "},
                new Answer{ID=24,Description="By a system in which there is no equality of opportunity"},
            };

            answers.ForEach(a => context.Answers.Add(a));
            context.SaveChanges();

            var questions = new List<Question>
            {

             new Question{ID=1, Description="Indicate which of these images is a woman", CorrectAnswerID=3},
                new Question{ID=2, Description="Indicate which of these images there is a man", CorrectAnswerID=5},
                new Question{ID=3, Description="Indicate which of these images represents a family", CorrectAnswerID=7},
                new Question{ID=4, Description="Indicates which of these images represents a mother roles", CorrectAnswerID=11},
                new Question{ID=5, Description="Indicate which of these images represents a girls´toys", CorrectAnswerID=15},
                new Question{ID=6, Description="Indicate which of these images show male attitudes", CorrectAnswerID=16},
                new Question{ID=7, Description="Indicate which of these images represents a boys´toys", CorrectAnswerID=20},
                new Question{ID=8, Description="The women, half of the world's population, are now a minority in the areas in which decisions of weight and where it is thought our time (parliaments, Governments, science, media, multinational companies, technology, art, philosophy, literature) why you think so?", CorrectAnswerID=23},
            };

            questions[0].Answers.Add(answers[0]);
            questions[0].Answers.Add(answers[1]);
            questions[0].Answers.Add(answers[2]);
            questions[1].Answers.Add(answers[3]);
            questions[1].Answers.Add(answers[4]);
            questions[1].Answers.Add(answers[5]);
            questions[2].Answers.Add(answers[6]);
            questions[2].Answers.Add(answers[7]);
            questions[2].Answers.Add(answers[8]);
            questions[3].Answers.Add(answers[9]);
            questions[3].Answers.Add(answers[10]);
            questions[3].Answers.Add(answers[11]);
            questions[4].Answers.Add(answers[12]);
            questions[4].Answers.Add(answers[13]);
            questions[4].Answers.Add(answers[14]);
            questions[5].Answers.Add(answers[15]);

            questions.ForEach(q => context.Questions.Add(q));
            context.SaveChanges();
        }
    }
}